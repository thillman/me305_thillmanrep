# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 12:54:15 2021

@author: melab15
"""

def fib_function(usern): 
#This is the start of my function to calculate the Fib Number
    
    if usern == 0: 
    #First I check if the index is 1 or 0; Two special cases where 
    #it is easier to simply store the answer rather than calculate it
        fna=0
    elif usern == 1:
        fna=1
    
    else:
    #Here I am starting the actual Fibonnaci calculation   
        idx=1
        #This is the index for the while loop
        fn1=0
        #This is the first stored number for the calculation
        fn2=1
        #This is the second stored number for the calculation
        while idx < usern:
        #The loop will run until the calculation index becomes larger than
        #the user inputted index
            idx=idx+1
            #on each iteration the index is increased...
            fna=fn1+fn2
            #the fibonnaci number is calculated...
            fn1=fn2
            fn2=fna
            #and the two numbers needed to calculate the next fibnoccci
            #number are updated.
        else:
            fna=fna
    return fna
    #Here the final fibonacci number is returned from the function

if __name__ == '__main__':
#Start of the main code
    
    while True:
    #Here I start a infinite while loop
        user = input('Please enter an index to calculate a Fibonnaci Number: ')
        #The user is prompted to input an index
        try:
        #it will try to run this portion of code and if there is an
        #error message it skips to the 'Except' if there is an error 
        #or else if there is no error
            usern=int(user)
            #I am using this line to verify that the input is an integer
            if usern < 0:
            #I am using this line to verify that the integer is positive
                print('Please enter a positive integer :)')
                #This is the error message that the user sees
                continue
                #if the error occurs 'continue' sends the code to the
                #start of the while loop
        except: 
            print('Please enter a positive integer :)')
            #This is the error message that the user sees
            continue
            #if the error occurs 'continue' sends the code to the
            #start of the while loop
        else:
    
            answer = fib_function(usern)
            #answer stores the returned 'fna' value from the function
            print('The Fibonnaci Number for',usern,'is:',answer)
            #The answer is formatted and printed to the user
            check = input('Enter q to quit or c to continue: ')
            #A simple continue or stop message is sent to user
            if check == ('q'):
                break
                #if the user enters q the while loop will break and 
                #end the code
            else:
                continue
                #if the user enters c the while loop restarts
    print('Ok, bye :)')
    #A message is sent indicating the program is done    
       

  