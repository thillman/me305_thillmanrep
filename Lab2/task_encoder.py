# -*- coding: utf-8 -*-
"""
    @file       task_encoder.py
    @brief      ---Insert brief here---
    @details    ---Insert Details on the class here---
    @author     Aleya Dolorofino
    @author     Tanner Hillman
    @date       10/21/21
"""

import utime
## imports the time module

import Encoder
## imports the encoder class 

import pyb
## imports the pyb module

pinA = pyb.Pin(pyb.Pin.cpu.B6)
##Sets pinA to pin B6 on the circuit board which communicates with the first
#encoder

pinB = pyb.Pin(pyb.Pin.cpu.B7)
##Sets pinB to pin B7 on the circuit board which communicates with the first
#encoder

tim_num = 4
##Sets the timer number equal to 4

period = 65535
##Sets the period of the timer to 65535 ticks. Since our timer is a 16-bit 
#timer, and we want to count every tick to create the highest resolution on 
#encoder we set the period to 65535: The total number of possibilities in a 
#16-bit counter.

##Shouldn't we be defining period in main and passing it into the encoder task
#somehow

S0_INIT = 0
##No clue what this is doing

set_time = 30000
##No clue what this is doing


class Task_Encoder:
    '''
        @brief      ---Insert brief here---
        @details    ---Insert details here---
    '''
    
    def __init__(self):
        '''
            @brief ---Insert brief here---
        '''
        self.state = S0_INIT
        self.period = period
        self.encoder1 = Encoder.Encoder(pinA, pinB, tim_num)
        self.data_list = []
        self.collect = False
        ##No clue what any of these ^^^^^ are doing 
       
    def run(self, KeyCommand):
        '''
            @brief  ---Insert brief here---
        '''
        
        timer = utime.ticks_ms()
        ##Sets timer equal to the output of the method "ticks_ms" of the utime
        #module
        
        if (KeyCommand == b'z'):
            self.encoder.set_position(0)
            print('The position has been zeroed.')
      
        elif (KeyCommand == b'p'):
            print('The current position is: ' + self.encoder.get_position())
            
        elif (KeyCommand == b'd'):
            print('The delta is: ' + self.encoder.get_delta())
        
        elif (KeyCommand == b'g'):
            self.collect = True  
            self.start_time = timer
            while (abs(self.start_time - timer) <= set_time):
                self.data_list.append(self.encoder.get_position())
                if (KeyCommand == b's'):
                    self.collect = False
                    break
            self.collect = False
            print('Here is your collected data')
            for n in range(len(self.data_list)):
                print(self.data_list[n])
           