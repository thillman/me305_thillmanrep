# -*- coding: utf-8 -*-
"""
    @file       main.py
    @brief      High-Level program responsible for running tasks.
    @details    ---input details here--- 
    @author     Tanner Hillman
    @author     Aleya Dolorofino
    @date       10/21/21
"""

import Task_User 
## imports the user task 
import Task_Encoder
## imports the encoder task 


if __name__ =='__main__':
##What does this do???
   
    encoderTask = Task_Encoder.Task_Encoder()
    ##Creates an object ("encoderTask") of the Task_Encoder class
    userTask = Task_User.Task_User()
    ##Creates an object ("userTask") of the Task_User class
    NeedCommand = True
    ##NeedCommand is a variable used to operate the FSM
         
    while (True):
        ##Creates continuous loop
        
        try:
        ##Program will try to run the following lines of indented code
            
            if NeedCommand:
            ##Will run if NeedCommand is set to true
                keyCommand = userTask.run()
                ##KeyCommand is a variable corresponding to a key input by the
                #user. KeyCommand is the output of the method "run" of class 
                #Task_User
                NeedCommand = encoderTask.run(keyCommand)
                ##NeedCommand is a variable corresponding to the output of the
                #method "run" of class Task_Encoder, for which KeyCommand is 
                #passed in.
                
                
                ##Why are we setting NeedCommand to the output of 
                #encoderTask.run here? There is no boolean output for the 
                #encoder task so I don't know how this will work?
            
        except KeyboardInterrupt:
        ##The continuous loop will run until a keyboard interuppt is deteced
        #from the user
            break
        
    print('Program Terminating')
    #A goodbye message is printed indicating to the user that the program has
    #ended
