# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 17:46:09 2021

@author: arocha19
"""
import pyb
import Encoder

CommReader = pyb.USB_VCP()

S0_INIT = 0
S1_WAIT_FOR_INPUT = 1
pinA = pyb.Pin(pyb.Pin.cpu.B6)
pinB = pyb.Pin(pyb.Pin.cpu.B7)
tim_num = 4

class Task_User:
    ''' @brief                  Interface with quadrature encoders
        @details
    '''
    
    def __init__(self):

        ''' 
        @brief              Constructs an encoder object
        @details
        
        '''
        ## The Current state for this iterations of the FSM
        self.state = S0_INIT
        self.encoder = Encoder.Encoder(pinA, pinB, tim_num)

    def transition_to(self, new_state):
        self.state = new_state;
        
    def run(self):
        
        if(self.state == S0_INIT):
            self.encoder.update()
            print ("INSTRUCTIONS:\n"
                  "z:   Zero the position of encoder\n"
                  "p:   Print out the position of encoder\n"
                  "d:   Print out the delta for encoder\n"
                  "g:   Collect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list\n"
                  "s:   End data collection prematurely\n"
                  "esc: Redisplay user command interface")
            
        elif(self.state == S1_WAIT_FOR_INPUT):
            if(CommReader.any()):
                #Reads Most recent Command
                keyCommand = CommReader.read(1).decode()
                
                # Clears Queue
                CommReader.read()
                
                # Goes back to State 0 the initial state when the esc key is hit
                if(keyCommand == b'\x1b'):
                    self.transition_to(S0_INIT)
                    return ''
                
                return keyCommand
            return''